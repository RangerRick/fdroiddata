Categories:System
License:GPLv3
Web Site:
Source Code:https://github.com/gabm/TapAndTurn
Issue Tracker:https://github.com/gabm/TapAndTurn/issues

Auto Name:Tap 'n' Turn
Summary:Confirm before the screen orientation gets applied
Description:
Tired of enabling the screen rotation everytime you want landscape mode?
Automatic rotation always rotates the screen in the wrong moment? Then this App
is for you!

Instead of rotating the screen right away, it displays an icon which allows you
explicitly adjust the orientation.
.

Repo Type:git
Repo:https://github.com/gabm/TapAndTurn

Build:1.0.0,1
    commit=v1.0.0
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.0.0
Current Version Code:1
