Categories:Internet
License:Apache2
Web Site:
Source Code:https://github.com/VladThodo/behe-explorer
Issue Tracker:https://github.com/VladThodo/behe-explorer/issues

Auto Name:BeHe ExploreR
Summary:Browse the web
Description:
Simple, small and minimalistic internet browser made for experimenting and
learning. Anybody is welcome to participate to this project no matter if you are
a developer, designer, translator, tester..or just a simple user reporting a bug
:-).
.

Repo Type:git
Repo:https://github.com/VladThodo/behe-explorer

Build:1.0.0.2,2
    disable=builds, but wait for https://github.com/VladThodo/behe-explorer/issues/21
    commit=v1.0.0.3
    subdir=app
    gradle=yes
    prebuild=rm src/main/res/drawable/gear.ico

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.1
Current Version Code:2
